import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

constructor() { }

public getEnvironmentUrl(): string {
  //return environment.apiUrl;
  return 'http://www.ayushyaaclinic.com/api/public/';
}

public handleError(error: any): Promise<any> {
  console.error('An error occurred', error); // for demo purposes only
  return Promise.reject(error.message || error);
}

}
