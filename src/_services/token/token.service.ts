import { Injectable } from '@angular/core';
import { BaseService } from '../base.service';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TokenService extends BaseService{

constructor(private http: HttpClient) {
  super();
}

private tokenService = this.getEnvironmentUrl();
private getAllTokenUrl = this.tokenService + 'GetAllToken';

getAllToken(): Promise<any> {
  const url = `${this.getAllTokenUrl}`;
  return this.http.get(url)
    .toPromise()
    .then(response => response)
    .catch(this.handleError);
}

}
