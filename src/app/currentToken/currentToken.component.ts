import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/_services/token/token.service';

@Component({
  selector: 'app-currentToken',
  templateUrl: './currentToken.component.html',
  styleUrls: ['./currentToken.component.scss']
})
export class CurrentTokenComponent implements OnInit {

  public currentToken: any;
  public nextToken1: any;
  public nextToken2: any;

  constructor(private tokenService: TokenService) { }

  ngOnInit() {
    this.getData();
    setInterval(() => {
     this.getData();
    }, 10000);
  }

  getData() {
    this.tokenService.getAllToken().then(response => {
      if(response.pending.length > 0){
        this.nextToken1 = response.pending[0].token_number;
      }
      else{
        this.nextToken1 = "-----";
      }

      if(response.pending.length > 1){
        this.nextToken2 = response.pending[1].token_number;
      }
      else{
        this.nextToken2 = "-----";
      }
      
      if(response.current.length > 0){
        if(this.currentToken == response.current[0].token_number){
          
        }
        else{
          this.playAudio();
          this.currentToken = response.current[0].token_number;
        }
      }
      else{
        this.currentToken = "-----";
      }
    });
  }

  playAudio(){
    let audio = new Audio();
    audio.src = "../../assets/audio/speech.mp3";
    audio.load();
    audio.play();
  }

}
